import Rx from 'rxjs';

/* from() method converts:
    Array => Observable
    Promise => Observable
    Iterator => Observable
*/
//Case 1. Converting array data into Observable
console.log('Case 1. Converting array data into Observable');
const fibonacciArray = [5,11,25,37,52];
Rx.Observable.from(
     fibonacciArray
).subscribe(a=>console.log(a));
/*
======Output========
5
11
25
37
52
*/

//Case 2. Converting Promise into Observable
console.log('Case 2. Converting Promise into Observable');
const fibonacciPromise = new Promise(r=>r(fibonacciArray));
Rx.Observable.from(
     fibonacciPromise
).subscribe(a=>console.log(a));
/*
======Output========
[ 5, 11, 25, 37, 52 ]
*/

//Case 3. Converting Iterator into Observable
console.log('Case 3. Converting Iterator into Observable');
function * fibonacciGenerator () {
    let a = 1, b = 1;
    while (true) {
        let c = a+b;
        yield c;
        a = b;
        b = c;
    }
};

Rx.Observable.from(
       fibonacciGenerator()
)
    .take(7)                    // take() method is used to restrict emitting of element to a certain number
    .subscribe(a=>console.log(a));
/*
======Output========will top 7 values only since take(7) is used
2
3
5
8
13
21
34

*/
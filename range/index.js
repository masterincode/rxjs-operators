import Rx from 'rxjs';

const rangeObservable = Rx.Observable.range(0,5);
rangeObservable
    .subscribe(e=>console.log(`Range emitted ${e}`));

/*
=============Output======================

Edge Case 1]  range(0,5) will emit the number from 0 to 4

Range emitted 0
Range emitted 1
Range emitted 2
Range emitted 3
Range emitted 4

Edge Case 2]  range(1,5) will emit the number from 1 to 5

Range emitted 1
Range emitted 2
Range emitted 3
Range emitted 4
Range emitted 5


*/
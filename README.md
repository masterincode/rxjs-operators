# RxJS-Operators
  RxJS operators is very important section of Reactive Form

## Getting Started
 1. After cloning the repository
 2. install dependencies with `npm install.`
 3. Install babel-cli globablly with `npm install -g babel-cli`
 4. To run this file,just type `babel-node folder_Name`

 ## For Webpack
 1. `npm install -g --save-dev webpack webpack-dev-server`
 2. To run webpack related files just type `webpack-dev-server`


## Studying after long time
1. run command first
  
       npm install babel

2. You can use IntelliJ terminal or Git Bash terminal to run your all index file

       babel-node index
   - If you see below output,then you are ready to go !! 
     
         OPERATORS WORLD!!
    
3. To run any folder of rxjs operator

        babel-node folder_name  e.g badel-node range
